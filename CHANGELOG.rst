Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

Unreleased
----------

v1.5.0
----------

Added
~~~~~
- `grav_wave_events` property (list[str]) to instances of Locus.

v1.4.0
----------

Added
~~~~~
- ``antares_client.search.get_available_tags`` to get searchable tags.

v1.3.0
----------

Added
~~~~~~~
- `grav_wave_events` property (list[dict]) to instances of Alert.

Changed
~~~~~~~
- All calls to antares APIs will have a timeout of 60 seconds to prevent the client from hanging indefinitely if there are no responses.

v1.2.3
----------

Added
~~~~~~~
- `processed_at` property (datetime) to instances of Alert.

v1.2.2
----------

Changed
~~~~~~~
- Pinned python confluent_kafka library to 1.7.0 to stay with ANTARES version. This prevents deprecation of the TLS certificate lack of verification.

v1.2.1
----------

Fixed
~~~~~

- Documentation for models

v1.2.0
----------

Added
~~~~~

- Support for fetching catalog cross-matches for Loci

Changed
~~~~~~~

- Marked classes in `models.py` as public

v1.1.0
----------

Added
~~~~~

- `coordinates` property (astropy.coordinates.SkyCoord) to instances of Locus

v1.0.6
----------

Changed
~~~~~~~

- Fetch configuration details from the API's /client/config/streaming/default endpoint
  in support of the migration off of Confluent's managed Kafka.

v1.0.5
----------

Changed
~~~~~~~

- Use FQDN, kafka.antares.noirlab.edu, for Kafka connection.

v1.0.4
------

Fixed
~~~~~

- Sorts search results in descending order on the `newest_alert_observation_time`
  property (i.e. latest first).

v1.0.3
------

Fixed
~~~~~

- Set to ignore undefined fields in API payload (allows for decoupling
  version of client from API additions).

v1.0.2
------

Fixed
~~~~~

- Bug where consecutive calls to paginate over all resources at an endpoint repeatedly
  concatenates request query parameters to the URL

Changed
~~~~~~~

- `locus.alerts`, `locus.lightcurve`, `locus.catalogs` are lazy loaded from the API
- Marked private interfaces as private

v1.0.1
------

Fixed
~~~~~

- Removed unnecessarily strict dependency requirements

v1.0.0
------

Added
~~~~~

- Type signatures for most of the library
- ``antares_client.search.cone_search`` for cone searches
- ``antares_client.search.get_by_ztf_object_id`` for lookup by ZTF Object ID

Changed
~~~~~~~

- Removed support for Python 3.4, 3.5
- Interfaces with the API at https://api.antares.noirlab.edu
- Streaming client returns loci instead of alerts
- Search queries hit API directly and no longer need to be prepared server-side

v0.3.2
------

Added
~~~~~

- Better error handling for networking issues in the Client.

v0.3.1
------

Fixed
~~~~~

- Use `time.perf_counter` instead of `time.process_time` for tracking
  timeout values. Fixes #1, where polling a Kafka stream took much
  longer than the specified timeout.

v0.3.0
------

Added
~~~~~

- The ``search`` subcommand, for searching and downloading querysets from the
  ANTARES ElasticSearch database.

- Started using ``click`` for CLI tooling.

- Initial release of documentation.

Fixed
~~~~~

- Verification of SSL certs in requests to the ANTARES portal for thumbnails.

Changed
~~~~~~~

- Renamed CLI ``antares-client`` to ``antares stream``.

v0.2.2
------

Added
~~~~~

- Support for custom Kafka commit behavior.

v0.2.0
------

Added
~~~~~

- ``antares_client.thumbnails`` module for downloading alert thumbnail images.

v0.1.0
------

Fixed
~~~~~

- \#6: ``_locate_ssl_certs_file`` was called in the ``Client`` constructor even
  if an SSL cert path was provided.

v0.0.1
------

Added
~~~~~

- Initial release
