Acknowledgements
================

The ANTARES project has been supported by the National Science Foundation
through a cooperative agreement with the Association of Universities for
Research in Astronomy (AURA) for the operation of NOIRLab, through an NSF INSPIRE
grant to the University of Arizona (CISE AST-1344024, PI: R. Snodgrass), and
through a grant from the Heising-Simons Foundation.

ZTF is supported by National Science Foundation grant AST-1440341 and a
collaboration including Caltech, IPAC, the Weizmann Institute for Science, the
Oskar Klein Center at Stockholm University, the University of Maryland, the
University of Washington, Deutsches Elektronen-Synchrotron and Humboldt
University, Los Alamos National Laboratories, the TANGO Consortium of Taiwan,
the University of Wisconsin at Milwaukee, and Lawrence Berkeley National
Laboratories. Operations are conducted by COO, IPAC, and UW. 
